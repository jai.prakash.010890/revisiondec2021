package package2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestDeadReentrant {
    private static ReentrantLock lock1= new ReentrantLock();

    public static void main(String[] args) {
        final String resource1 = "Jai";
        final String resource2 = "Prakash";

        Resource1ExReentrant runnable1 = new Resource1ExReentrant(lock1,resource1, resource2);
        Resource2ExReentrant runnable2 = new Resource2ExReentrant(lock1,resource1, resource2);
        // t1 tries to lock resource1 then resource2
        Thread t1 = new Thread(runnable1);
        Thread t2 = new Thread(runnable2);

        t1.start();
        t2.start();

    }
}
