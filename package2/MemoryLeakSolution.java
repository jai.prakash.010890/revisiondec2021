package package2;

import package1.test.Key;

import java.util.HashMap;
import java.util.Map;

public class MemoryLeakSolution {

    static class Key {
        Integer id;

        Key(Integer id) {
            this.id = id;
        }

//        @Override
//        public int hashCode() {
//            return id.hashCode();
//        }
//
//        @Override
//        public boolean equals(Object obj)
//        {
//            if(this == obj)
//                return true;
//
//            if(obj == null || obj.getClass()!= this.getClass())
//                return false;
//
//            // type casting of the argument.
//            Key key = (Key) obj;
//
//            // comparing the state of argument with
//            // the state of 'this' Object.
//            return (key.id.equals(this.id));
//        }

    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Map m = new HashMap();
        while (true)
            for (int i = 0; i < 10000; i++)
                if (!m.containsKey(new MemoryLeakSolution.Key(i)))
                    m.put( "Number:" + i, new MemoryLeakSolution.Key(i));
    }
}
