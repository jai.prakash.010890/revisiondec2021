package package2;

import java.util.HashMap;
import java.util.Map;

public class MemoryLeak {

    static class Key {
        Integer id;

        Key(Integer id) {
            this.id = id;
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Map m = new HashMap();
        while (true)
            for (int i = 0; i < 10000; i++)
                if (!m.containsKey(i)) // m.contains(0)
                    m.put(new Key(i), "Number:" + i);

        /**
         * Key => object === new Key(0), 0
         * Value => integer
         */
    }
}