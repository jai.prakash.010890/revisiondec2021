package package2;

public class TestDead {
    public static void main(String[] args) {
        final String resource1 = "Jai";
        final String resource2 = "Prakash";

        Resource1Ex runnable1 = new Resource1Ex(resource1, resource2);
        Resource2Ex runnable2 = new Resource2Ex(resource1, resource2);
        // t1 tries to lock resource1 then resource2
        Thread t1 = new Thread(runnable1);
        Thread t2 = new Thread(runnable2);



        t1.start();
        t2.start();
    }
}
