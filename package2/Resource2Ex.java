package package2;

public class Resource2Ex implements Runnable{

    private String res1;
    private String res2;

    public Resource2Ex(String res1, String res2){
        this.res1= res1;
        this.res2 = res2;
    }

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();

        synchronized (res2){
            System.out.println("Thread 1: locked resource 1");

            try { Thread.sleep(100);} catch (Exception e) {}

            synchronized (res1) {
                System.out.println("Thread 1: locked resource 2");
            }
        }
    }
}
