package package2;

import java.util.concurrent.locks.ReentrantLock;

public class Resource2ExReentrant implements Runnable{

    private String res1;
    private String res2;
    private ReentrantLock lock;

    public Resource2ExReentrant(ReentrantLock lock, String res1, String res2){
        this.res1= res1;
        this.res2 = res2;
        this.lock = lock;
    }

    public void run() {
        System.out.println(res1);
        lock.lock();

    }


}
