package package2;

import java.sql.*;

public class UnclosedDBConnection {

    public static void main(String[] args) throws SQLException, InterruptedException {
		for(int i=0; i< 1000; i++){
			displayUsers();
//			Thread.sleep(500);
		}
    }

	static void displayUsers(){
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spring", "root", "password");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from user_tbl");
			while (rs.next()) {
//				System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
//				Thread.sleep(50);
			}
			 con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}