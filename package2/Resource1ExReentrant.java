package package2;

import java.util.concurrent.locks.ReentrantLock;

public class Resource1ExReentrant implements Runnable{

    private String res1;
    private String res2;
    private ReentrantLock lock;
    private ReentrantLock lock2;

    public Resource1ExReentrant(ReentrantLock lock,String res1, String res2){
        this.res1= res1;
        this.res2 = res2;
        this.lock = lock;
    }

    public void run() {
        lock.lock();
        System.out.println(res1);
        try { Thread.sleep(100);} catch (Exception e) {}
//        lock.unlock();
    }


}
