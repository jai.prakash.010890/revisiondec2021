package session3.nioclasses.usionnio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateDirectory {

    public static void main(String[] args) {
        try {
            Path p = Paths.get("testdir");
            if(Files.exists(p)){
                System.out.println("Directory exists");
            }else{
                Path path = Files.createDirectories(p);

                System.out.println("Directory Created at "+ path.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
