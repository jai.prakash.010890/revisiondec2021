package session3.nioclasses.usionnio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;


public class NonBlockingClient {
    private String REQUESTED_RESOURCE = "/";


    public void nioReadingWriting() throws IOException {
        // given a NIO SocketChannel and a charset
//        InetAddress address = new Ine
        Long startTime = System.currentTimeMillis();
        InetSocketAddress address = new InetSocketAddress("localhost", 9191);
        SocketChannel socketChannel = SocketChannel.open(address);
        Charset charset = StandardCharsets.UTF_8;

        // when we write and read using buffers
        socketChannel.write(charset.encode(CharBuffer.wrap("GET " + REQUESTED_RESOURCE + " HTTP/1.0\r\n\r\n")));

        ByteBuffer byteBuffer = ByteBuffer.allocate(8192); // or allocateDirect if we need direct memory access
        CharBuffer charBuffer = CharBuffer.allocate(8192);
        CharsetDecoder charsetDecoder = charset.newDecoder();
        StringBuilder ourStore = new StringBuilder();
        while (socketChannel.read(byteBuffer) != -1 || byteBuffer.position() > 0) {
            byteBuffer.flip();
            storeBufferContents(byteBuffer, charBuffer, charsetDecoder, ourStore);
            byteBuffer.compact();
        }
        socketChannel.close();
        Long endTime = System.currentTimeMillis();
        // then we read and saved our data
        System.out.println(ourStore.toString());
        System.out.println(endTime - startTime);
    }

    public static void main(String[] args) throws IOException {
        NonBlockingClient nonBlockingClient = new NonBlockingClient();
        nonBlockingClient.nioReadingWriting();
    }


    void storeBufferContents(ByteBuffer byteBuffer, CharBuffer charBuffer, CharsetDecoder charsetDecoder, StringBuilder ourStore) {
//        charsetDecoder.decode(byteBuffer, charBuffer, true);
//        charBuffer.flip();
//        ourStore.append(charBuffer);
//        charBuffer.clear();
    }
}