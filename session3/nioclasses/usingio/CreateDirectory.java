package session3.nioclasses.usingio;

import java.io.File;
public class CreateDirectory {
   public static void main(String args[]) {

      String path = "testdirio";
      //Creating a File object
      File file = new File(path);
      //Creating the directory
      boolean bool = file.mkdir();
      if(bool){
         System.out.println("Directory created successfully");
      }else{
         System.out.println("Sorry couldn’t create specified directory");
      }
   }
}