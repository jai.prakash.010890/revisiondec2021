package session3.nioclasses.usingio;



import java.io.*;
import java.net.Socket;

public class BlockingClient {
    private static final String REQUESTED_RESOURCE = "/";

    public void readingWritingWithStreams() throws IOException {
        // given an IO socket and somewhere to store our result
        Long startTime = System.currentTimeMillis();
        Socket socket = new Socket("localhost", 9191);
        StringBuilder ourStore = new StringBuilder();

        // when we write and read (using try-with-resources so our resources are auto-closed)
        try (InputStream serverInput = socket.getInputStream();
          BufferedReader reader = new BufferedReader(new InputStreamReader(serverInput));
          OutputStream clientOutput = socket.getOutputStream();
          PrintWriter writer = new PrintWriter(new OutputStreamWriter(clientOutput))) {
            writer.print("GET " + REQUESTED_RESOURCE + " HTTP/1.0\r\n\r\n");
            writer.flush(); // important - without this the request is never sent, and the test will hang on readLine()

            for (String line; (line = reader.readLine()) != null; ) {
                ourStore.append(line);
                ourStore.append(System.lineSeparator());
            }
        }

        // then we read and saved our data
        Long endTime= System.currentTimeMillis();
        // then we read and saved our data
        System.out.println(ourStore.toString());
        System.out.println(endTime- startTime);
    }

    public static void main(String[] args) throws IOException {
        BlockingClient blockingClient=new BlockingClient();
        blockingClient.readingWritingWithStreams();
    }
}