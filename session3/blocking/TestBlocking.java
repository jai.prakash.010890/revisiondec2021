package session3.blocking;

import java.io.*;
import java.net.Socket;

/**
 * io - blocking
 * nio - non blocking
 *
 */
public class TestBlocking {

    public static void main(String[] args) throws IOException {
        Long startTime = System.currentTimeMillis();
        String url = "/";
        Socket socket = new Socket("localhost", 8080);

        StringBuilder ourStore = new StringBuilder();

        try(InputStream serverInput = socket.getInputStream()){
            BufferedReader reader = new BufferedReader(new InputStreamReader(serverInput));

            OutputStream clientOutput = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(clientOutput));

            writer.print("GET " + url + " HTTP/1.0\r\n\r\n");

            writer.flush();

            for(String line; (line= reader.readLine())!=null;){
                ourStore.append(line);
                ourStore.append(System.lineSeparator());
            }

            System.out.println(ourStore.toString());
        }

        Long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);
    }
}
