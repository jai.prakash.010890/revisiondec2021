package session3;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class RequestStringBuilder {

    public static String getRequestString(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder request = new StringBuilder();

        for(Map.Entry<String, String> entry: params.entrySet()){
            request.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            request.append("=");
            request.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            request.append("&");
        }

        String requestString = request.toString();
        return requestString.length() > 0 ? requestString.substring(0, requestString.length()-1) : requestString;
    }
}
