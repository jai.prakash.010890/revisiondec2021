package session3.threads;

import java.util.concurrent.locks.ReentrantLock;

public class TestDeadLockDifferentClasses {

    public static void main(String[] args) {
        final String resource1 = "Jai";
        final String resource2 = "Prakash";

        ReentrantLock lock = new ReentrantLock();

        Resource1 runnable1 = new Resource1(lock,resource1, resource2);
//        Resource2 runnable2 = new Resource2(resource1, resource2);

        Thread t1 = new Thread(runnable1);
//        Thread t2 = new Thread(runnable2);

        t1.start();
//        t2.start();
    }
}
