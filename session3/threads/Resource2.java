package session3.threads;

public class Resource2 implements Runnable{
    private String resource1;
    private String resource2;

    public Resource2(String resource1, String resource2) {
        this.resource1 = resource1;
        this.resource2 = resource2;
    }

    @Override
    public void run() {
        synchronized (resource2){
            System.out.println(Thread.currentThread().getName());

            synchronized (resource1){

            }
        }
    }
}
