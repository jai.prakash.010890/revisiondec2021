package session3.threads;

import java.util.concurrent.locks.ReentrantLock;

public class Resource1 implements Runnable{

    private String resource1;
    private String resource2;
    private ReentrantLock lock;

    public Resource1(ReentrantLock lock,String resource1, String resource2) {
        this.resource1 = resource1;
        this.resource2 = resource2;
        this.lock = lock;
    }

    @Override
    public void run() {
        lock.lock();
        for(int i=0; i< 10; i++){
            if(lock.tryLock()){
                System.out.println(lock.getHoldCount());
            }
            System.out.println(resource1);
        }


        lock.unlock();

    }
}
