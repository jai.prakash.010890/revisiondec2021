package session3.threads;

public class TestDeadlockSameClass {
  public static void main(String[] args) {  
    final String resource1 = "Jai";
    final String resource2 = "Prakash";

    Thread t1 = new Thread() {
      public void run() {  
        synchronized (resource1){
          System.out.println(Thread.currentThread().getName());

          synchronized (resource2){

          }
        }
      }  
    };  
  
    // t2 tries to lock resource2 then resource1  
    Thread t2 = new Thread() {  
      public void run() {
        synchronized (resource1){
          System.out.println(Thread.currentThread().getName());

          synchronized (resource2){

          }
        }
      }  
    };  

    t1.start();  
    t2.start();  
  }  
}       
