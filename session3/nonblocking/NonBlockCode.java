package session3.nonblocking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;

public class NonBlockCode {

    public void nioReadingWriting() throws IOException {
        String url ="/";
        InetSocketAddress address = new InetSocketAddress("localhost", 8080);

        SocketChannel socketChannel = SocketChannel.open(address);

        Charset charset = StandardCharsets.UTF_8;

        socketChannel.write(charset.encode(CharBuffer.wrap("GET " + url + " HTTP/1.0\r\n\r\n")));

        ByteBuffer byteBuffer= ByteBuffer.allocate(8192);

        CharBuffer charBuffer = CharBuffer.allocate(8192);

        CharsetDecoder charsetDecoder = charset.newDecoder();

        StringBuilder ourStore = new StringBuilder();

        while (socketChannel.read(byteBuffer) != -1 || byteBuffer.position() > 0){
            byteBuffer.flip();
            storeBuffer(byteBuffer, charBuffer, charsetDecoder, ourStore);
            byteBuffer.compact();

        }

        socketChannel.close();

        System.out.println(ourStore.toString());

    }

    void storeBuffer(ByteBuffer byteBuffer, CharBuffer charBuffer, CharsetDecoder charsetDecoder, StringBuilder ourStore){
        charsetDecoder.decode(byteBuffer, charBuffer, true);
        charBuffer.flip();
        ourStore.append(charBuffer);
        charBuffer.clear();
    }

    public static void main(String[] args) throws IOException {
        NonBlockCode nonBlockCode = new NonBlockCode();
        nonBlockCode.nioReadingWriting();
    }
}
